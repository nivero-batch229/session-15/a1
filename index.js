console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let firstName = "First Name: Love Arn";
	let lastName = "Last Name: Lang";
	console.log(firstName);
	console.log(lastName)

	let ageInitial = "Age: ";
	let ageNumber = 25;
	console.log(ageInitial + ageNumber);


	let hobby = "Hobbies: ";
	console.log(hobby)

	let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
	console.log(hobbies);

	let workPlace = "Work Address: ";
	let workDetails = {
		adress: {
			houseNumber: "213",
			street: "Washington",
			city: "Rodriguez",
			state: "Philippines",
		}
	}
	console.log(workPlace);
	console.log(workDetails)

	let fullName = "Love Arn Lang";
	console.log("My full name is" + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce", "Thor", "Natasha","Clint" , "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "ako_si_darna",
		endName: "Love Arn Lang",
		age: 25,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

